angular.module('starter.controllers', [])


//Splash Screen  in controller
.controller('SplashscreenController', function($scope,$http,$ionicLoading,$ionicHistory){

  $scope.data = {};
  $scope.data.bgColors = [];
  $scope.data.currentPage = 0;
  $scope.demostatus = "block";

  for (var i = 0; i < 2; i++) {
    $scope.data.bgColors.push("bgColor_" + i);
  }

  var setupSlider = function() {
    //some options to pass to our slider
    $scope.data.sliderOptions = {
      initialSlide: 0,
      direction: 'horizontal', //or vertical
      speed: 300,//0.3s transition
      pagination: false
    };

    //create delegate reference to link with slider
    $scope.data.sliderDelegate = null;

    //watch our sliderDelegate reference, and use it when it becomes available
    $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
      if (newVal != null) {
        $scope.data.sliderDelegate.on('slideChangeStart', function() {
          $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
          console.log($scope.data.sliderDelegate.activeIndex)
          //use $scope.$apply() to refresh any content external to the slider
          if($scope.data.sliderDelegate.activeIndex === 1){
          	$scope.demostatus="none"
          	$scope.languagestatus ="block"
          }
          if($scope.data.sliderDelegate.activeIndex === 0){
          		$scope.demostatus="block"
          		$scope.languagestatus ="none"
          }
          $scope.$apply();
        });
      }
    });
  };

  setupSlider();
})




//Login controller
.controller('LoginController', function($scope,myService,$http,$location,$state){
  

})

//Sign up controller
.controller('SignupController', function($scope,$http,$ionicLoading,$ionicHistory){
	//goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
   
})



//Sign in controller
.controller('SigninController', function($scope,$http,$ionicLoading,$ionicHistory){
   
    //goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
})



//forget password controller
.controller('ForgetpasswordController', function($scope,$http,$ionicLoading,$ionicHistory){

	//goback
	$scope.myGoBack = function(){
		console.log("Hey")
		$ionicHistory.goBack();
	}
})


// menu controller
.controller('MenuController', function($scope,$http,$ionicLoading,$ionicHistory,$ionicSideMenuDelegate){
  

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };


  $scope.menu = function(){
    console.log("Clicked")
  }

})


//plumbing controller
.controller('PlumbingController', function($scope,myService,$http,$location,$state){
  

})



//VendorController
.controller('VendorController', function($scope,myService,$http,$location,$state){
  

})



.controller('SubcategoriesController', function($scope,myService,$http,$location,$state){
  

})




