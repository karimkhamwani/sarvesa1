angular.module('starter', ['ionic','starter.controllers','starter.services','ngCordova']) 

.run(function($ionicPlatform,$cordovaStatusbar,$rootScope,$ionicHistory) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      //StatusBar.styleDefault();
      StatusBar.overlaysWebView(false) 
      StatusBar.backgroundColorByName("purple")
      // statusBar.backgroundColorByHexString('#662d91');
    }
    $ionicPlatform.onHardwareBackButton(function() {
    if ($ionicHistory.currentStateName() == 'Login'){
      ionic.Platform.exitApp();
    } else {
      $ionicHistory.goBack();
    }
    })

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('Login', {
        url: '/Login',
        templateUrl: 'views/Login.html',
        controller: 'LoginController'
      })
   .state('Signup', {
        url: '/Signup',
        templateUrl: 'views/Signup.html',
        controller: 'SignupController'
      })
   .state('Signin', {
        url: '/Signin',
        templateUrl: 'views/Signin.html',
        controller: 'SigninController'
      })
     .state('Forgetpassword', {
        url: '/Forgetpassword',
        templateUrl: 'views/Forgetpassword.html',
        controller: 'ForgetpasswordController'
      })
    .state('Splashscreen', {
        url: '/Splashscreen',
        templateUrl: 'views/Splashscreen.html',
        controller: 'SplashscreenController'
      })
    .state('Menu', {
        url: '/Menu',
        templateUrl: 'views/Menu.html',
        controller: 'MenuController'
      })
    .state('Plumbing', {
        url: '/Plumbing',
        templateUrl: 'views/Plumbing.html',
        controller: 'PlumbingController'
      })
    .state('Vendor', {
        url: '/Vendor',
        templateUrl: 'views/Vendor.html',
        controller: 'VendorController'
      })

   

  $urlRouterProvider.otherwise('/Splashscreen');
})
